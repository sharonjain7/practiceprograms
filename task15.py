#types of loops 
#For loop, While loop, A for loop is used for iterating over a sequence (that is either a list, a tuple, a dictionary, a set, or a string).

#This is less like the for keyword in other programming languages, and works more like an iterator method as found in other object-orientated programming languages.

#With the for loop we can execute a set of statements, once for each item in a list, tuple, set etc.

#Example
#Print each fruit in a fruit list:

fruits = ["apple", "banana", "cherry"]
for x in fruits:
  print(x)
#the for loop does not require an indexing variable to set beforehand.

#Looping Through a String
#Even strings are iterable objects, they contain a sequence of characters:

#Example
#Loop through the letters in the word "banana":

for x in "banana":
  print(x)
#The break Statement
#With the break statement we can stop the loop before it has looped through all the items:

#Example
#Exit the loop when x is "banana":

fruits = ["apple", "banana", "cherry"]
for x in fruits:
  print(x)
  if x == "banana":
    break
#Example
#Exit the loop when x is "banana", but this time the break comes before the print:

fruits = ["apple", "banana", "cherry"]
for x in fruits:
  if x == "banana":
    break
  print(x)

#The continue Statement
#With the continue statement we can stop the current iteration of the loop, and continue with the next:

#Example
#Do not print banana:

fruits = ["apple", "banana", "cherry"]
for x in fruits:
  if x == "banana":
    continue
  print(x)
#The range() Function
#To loop through a set of code a specified number of times, we can use the range() function,
#The range() function returns a sequence of numbers, starting from 0 by default, and increments by 1 (by default), and ends at a specified number.

#Example
#Using the range() function:

for x in range(6):
  print(x)
##Note that range(6) is not the values of 0 to 6, but the values 0 to 5.

#he range() function defaults to 0 as a starting value, however it is possible to specify the starting value by adding a parameter: range(2, 6), which means values from 2 to 6 (but not including 6):

#Example
#Using the start parameter:

for x in range(2, 6):
  print(x)
#The range() function defaults to increment the sequence by 1, however it is possible to specify the increment value by adding a third parameter: range(2, 30, 3):

#Example
#Increment the sequence with 3 (default is 1):

for x in range(2, 30, 3):
  print(x)
#Else in For Loop
#The else keyword in a for loop specifies a block of code to be executed when the loop is finished:

#Example
#Print all numbers from 0 to 5, and print a message when the loop has ended:

for x in range(6):
  print(x)
else:
  print("Finally finished!")
#Note: The else block will NOT be executed if the loop is stopped by a break statement.

#Example
#Break the loop when x is 3, and see what happens with the else block:

for x in range(6):
  if x == 3: break
  print(x)
else:
  print("Finally finished!")
#Nested Loops
#A nested loop is a loop inside a loop.

#The "inner loop" will be executed one time for each iteration of the "outer loop":

#Example
#Print each adjective for every fruit:

adj = ["red", "big", "tasty"]
fruits = ["apple", "banana", "cherry"]

for x in adj:
  for y in fruits:
    print(x, y)
#The pass Statement
#for loops cannot be empty, but if you for some reason have a for loop with no content, put in the pass statement to avoid getting an error.

#Example
for x in [0, 1, 2]:
  pass


#Range function 

#Concatenation of two range() functions
#The result from two range() functions can be concatenated by using the chain() method of itertools module. The chain() method is used to print all the values in iterable targets one after another mentioned in its arguments.

# Python program to concatenate 
# the result of two range functions 
  
  
from itertools import chain 
  
  
# Using chain method 
print("Concatenating the result") 
res = chain(range(5), range(10, 20, 2)) 
  
for i in res: 
    print(i, end=" ") 

#Using else Statement with Loops
#Python supports having an else statement associated with a loop statement.

#If the else statement is used with a for loop, the else statement is executed when the loop has exhausted iterating the list.

#If the else statement is used with a while loop, the else statement is executed when the condition becomes false.

#The following example illustrates the combination of an else statement with a while statement that prints a number as long as it is less than 5, otherwise the else statement gets executed.

#Example

#!/usr/bin/python3

count = 0
while count < 5:
   print (count, " is  less than 5")
   count = count + 1
else:
   print (count, " is not less than 5")


#The continue statement in Python returns the control to the beginning of the current loop. When encountered, the loop starts next iteration without executing the remaining statements in the current iteration.

#The continue statement can be used in both while and for loops.

#Syntax
#continue


for letter in 'Python':     # First Example
   if letter == 'h':
      continue
   print ('Current Letter :', letter)

var = 10                    # Second Example
while var > 0:              
   var = var -1
   if var == 5:
      continue
   print ('Current variable value :', var)
print ("Good bye!")


#Do While in python
#Emulating do-while in Python
#We can write the equivalent for the do-while in the above C program using a while loop, in Python as follows:

i = 1

while True:
    print(i)
    i = i + 1
    if(i > 3):
        break

#Print the following pattern
#1 
#1 2 
#1 2 3 
#1 2 3 4 
#1 2 3 4 5

for i in range(1,6):
  for j in range(1,i+1):
    print(j,end='')
  print("\n")




