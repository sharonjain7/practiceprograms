import re

#program to find all the numbers in a string

ques=input("Enter a string")

x=re.findall("\d",ques)

if x:

    print("The string contains the following numbers:",x)

#program to check whether string starts and ends with same charcter or not

ques2=input("Please enter a string")

start=re.findall('^[a-z]',ques2)
end=re.findall("[a-z]$",ques2)

if start==end:
    print("The start character is similar to the end character")

else:
    print("The start and the end of this string are different")
