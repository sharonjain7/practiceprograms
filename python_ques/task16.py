#Docstrings in python 

def add_binary(a, b):
    '''
    Returns the sum of two decimal numbers in binary digits.

            Parameters:
                    a (int): A decimal integer
                    b (int): Another decimal integer

            Returns:
                    binary_sum (str): Binary string of the sum of a and b
    '''
    binary_sum = bin(a+b)[2:]
    return binary_sum


print(add_binary.__doc__)





#SLICING LISTS IN PYTHON
'''In Python, list slicing is a common practice and it is the most used technique for programmers to solve efficient problems. Consider a python list, In-order to access a range of elements in a list, you need to slice a list. One way to do this is to use the simple slicing operator i.e. colon(:)

#With this operator, one can specify where to start the slicing, where to end, and specify the step. List slicing returns a new list from the existing list.'''

'''#Syntax:

#Lst[ Initial : End : IndexJump ]



#slice() Constructor
#The slice() constructor creates a slice object representing the set of indices specified by range(start, stop, step).

#Syntax:

#slice(stop)
#slice(start, stop, step)'''

'''Extending indexing
In Python, indexing syntax can be used as a substitute for the slice object. This is an easy and convenient way to slice a string both syntax wise and execution wise.

Syntax

string[start:end:step]
start, end and step have the same mechanism as slice() constructor.'''





#SPLIT FUNCTION
'''Python String split()
The split() method breaks up a string at the specified separator and returns a list of strings.


The syntax of split() is:

str.split([separator [, maxsplit]])
split() Parameters
split() method takes a maximum of 2 parameters:

separator (optional)- It is a delimiter. The string splits at the specified separator.
If the separator is not specified, any whitespace (space, newline etc.) string is a separator.
maxsplit (optional) - The maxsplit defines the maximum number of splits.
The default value of maxsplit is -1, meaning, no limit on the number of splits.
Return Value from split()
split() breaks the string at the separator and returns a list of strings.

Example 1: How split() works in Python?
text= 'Love thy neighbor'

# splits at space
print(text.split())

grocery = 'Milk, Chicken, Bread'

# splits at ','
print(grocery.split(', '))

# Splitting at ':'
print(grocery.split(':'))
Output

['Love', 'thy', 'neighbor']
['Milk', 'Chicken', 'Bread']
['Milk, Chicken, Bread']
Example 2: How split() works when maxsplit is specified?
grocery = 'Milk, Chicken, Bread, Butter'

# maxsplit: 2
print(grocery.split(', ', 2))

# maxsplit: 1
print(grocery.split(', ', 1))

# maxsplit: 5
print(grocery.split(', ', 5))

# maxsplit: 0
print(grocery.split(', ', 0))


Output

['Milk', 'Chicken', 'Bread, Butter']
['Milk', 'Chicken, Bread, Butter']
['Milk', 'Chicken', 'Bread', 'Butter']
['Milk, Chicken, Bread, Butter']
If maxsplit is specified, the list will have the maximum of maxsplit+1 items.'''


#JOIN FUNCTION

'''join() function in Python
Difficulty Level : Basic
 Last Updated : 02 Jan, 2018

The join() method is a string method and returns a string in which the elements of sequence have been joined by str separator.

Syntax:

string_name.join(iterable) 

string_name: It is the name of string in which
             joined elements of iterable will be
             stored.
Parameters: The join() method takes iterable – objects capable of returning its members one at a time. Some examples are List, Tuple, String, Dictionary and Set

Return Value: The join() method returns a string concatenated with the elements of iterable.

Type Error: If the iterable contains any non-string values, it raises a TypeError exception.




# Python program to demonstrate the 
# use of join function to join list 
# elements with a character. 
  
list1 = ['1','2','3','4']  
  
s = "-"
  
# joins elements of list1 by '-' 
# and stores in sting s 
s = s.join(list1) 
  
# join use to join a list of 
# strings to a separator s 
print(s) 
Output:

1-2-3-4'''





#SETS IN PYTHON

'''
Creating Python Sets
A set is created by placing all the items (elements) inside curly braces {}, separated by comma, or by using the built-in set() function.

It can have any number of items and they may be of different types (integer, float, tuple, string etc.). But a set cannot have mutable elements like lists, sets or dictionaries as its elements.

# Different types of sets in Python
# set of integers
my_set = {1, 2, 3}
print(my_set)

# set of mixed datatypes
my_set = {1.0, "Hello", (1, 2, 3)}
print(my_set)
Output

{1, 2, 3}
{1.0, (1, 2, 3), 'Hello'}
Try the following examples as well.

# set cannot have duplicates
# Output: {1, 2, 3, 4}
my_set = {1, 2, 3, 4, 3, 2}
print(my_set)

# we can make set from a list
# Output: {1, 2, 3}
my_set = set([1, 2, 3, 2])
print(my_set)

# set cannot have mutable items
# here [3, 4] is a mutable list
# this will cause an error.

my_set = {1, 2, [3, 4]}
Output

{1, 2, 3, 4}
{1, 2, 3}
Traceback (most recent call last):
  File "<string>", line 15, in <module>
    my_set = {1, 2, [3, 4]}
TypeError: unhashable type: 'list'
Creating an empty set is a bit tricky.

Empty curly braces {} will make an empty dictionary in Python. To make a set without any elements, we use the set() function without any argument.

# Distinguish set and dictionary while creating empty set

# initialize a with {}
a = {}

# check data type of a
print(type(a))

# initialize a with set()
a = set()

# check data type of a
print(type(a))
Output

<class 'dict'>
<class 'set'>'''


'''Modifying a set in Python
Sets are mutable. However, since they are unordered, indexing has no meaning.

We cannot access or change an element of a set using indexing or slicing. Set data type does not support it.

We can add a single element using the add() method, and multiple elements using the update() method. The update() method can take tuples, lists, strings or other sets as its argument. In all cases, duplicates are avoided.

# initialize my_set
my_set = {1, 3}
print(my_set)

# my_set[0]
# if you uncomment the above line
# you will get an error
# TypeError: 'set' object does not support indexing

# add an element
# Output: {1, 2, 3}
my_set.add(2)
print(my_set)

# add multiple elements
# Output: {1, 2, 3, 4}
my_set.update([2, 3, 4])
print(my_set)

# add list and set
# Output: {1, 2, 3, 4, 5, 6, 8}
my_set.update([4, 5], {1, 6, 8})
print(my_set)
Output

{1, 3}
{1, 2, 3}
{1, 2, 3, 4}
{1, 2, 3, 4, 5, 6, 8}'''
'''

#iterating through a set
for i in set("kada lala tata"):
    print(i)'''

