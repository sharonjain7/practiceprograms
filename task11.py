#data types in python 
#numeric data types
a=5
b=5.67
c=3+7j

print(type(a))
print(type(b))
print(type(c))

#strings
d='hi my name is sharon'
print(d)
print(d+d)
print(d*3)
print(d[3:6])

#lists

lis= [1,2,3,4]
print(lis[0:2])

#tuple
tup=(1,'ruru','hi',2)
print(tup)
print(tup[0:1])

#dictionary
dic={1:[1,2,3],2:[2,3],3:[3,4]}
print(dic)
print(dic.keys())
print(dic.values())

print(sum(dic.keys()))

dic[2]="hi"
print(dic)

#accessing lists by dictname[keyname]
print(dic[1]+dic[3])
mydic=dic.copy()
print(mydic)


#updating dictionary
dic.update({'2':"hihihi"})


#looping through dictionary
dictt={1:"pasta", 2:"pizza", 3:"dinner"}

for x in dictt.keys():
    print(dictt[x])

for x in dictt.keys():
    print(x)


#copy dictionary

new=dictt.copy()
print(new

)


#lists
list1=["hi","my","name","is","sharon"]

for x in list1:
    lis.append(x)

print(lis)    


#no repeatition in lists

list2= [3,2,2,3]
lis=[]

def repeat(s,g):
    i=0
    while i<len(s):
        if s[i] not in g:
            g.append(s[i])
        i=i+1 
        print(g)

repeat(list2,lis)


